const { PHASE_DEVELOPMENT_SERVER } = require('next/constants') 

module.exports = (phase) => {
  const isDev = phase === PHASE_DEVELOPMENT_SERVER

  const env = {
    SHOW_DEBUG_PANEL : (() => isDev)()
  }
  return {
    env,
    reactStrictMode: true,
  }
};
