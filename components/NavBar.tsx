import { useState } from "react";
import tw, { css } from "twin.macro";
import NavigationToggle from "./NavigationToggle";
import Link from "next/link";

export default function Navbar() {
  const [collapsed, setCollapsed] = useState(true);

  return (
    <nav tw="relative w-full container mx-auto bg-white flex flex-none py-2 px-4 items-center justify-between lg:justify-start z-10">
      <Link href="/">
        <a>
          <img
            tw="w-auto h-[35px] lg:h-[45px] z-[1]"
            width="197"
            height="45"
            src="https://d3hnd77n22bg9c.cloudfront.net/c/tardis/197w/RS8402_grand-canyon-scenic-airlinesLOGO-01.png"
            alt="Grand Canyon Scenic Airlines"
          />
        </a>
      </Link>
      <div
        css={[
          collapsed && tw`hidden`,
          tw`fixed flex-grow top-0 inset-x-0 pt-[69px] z-[-1] h-screen bg-white`,
          tw`lg:(relative flex inset-x-auto pt-0 z-0 h-auto bg-transparent)`,
        ]}
      >
        <div tw="relative h-full lg:ml-auto">
          <ul
            role="menubar"
            tw="flex flex-col lg:flex-row text-[#3394b1] font-bold uppercase px-4 pt-4 lg:p-0"
          >
            <MenuItem url="#" label="About" />
            <MenuItem url="#" label="Tours" />
            <MenuItem url="#" label="Flights" />
            <MenuItem url="#" label="Articles and Advice" />
          </ul>
        </div>
      </div>
      <div></div>
      <NavigationToggle
        active={!collapsed}
        onClick={() => setCollapsed(!collapsed)}
      />
    </nav>
  );
}

function MenuItem({ url, label }) {
  return (
    <li role="none">
      <Link href={url}>
        <a tw="flex flex-grow p-2 lg:py-4">{label}</a>
      </Link>
    </li>
  );
}
