import tw, { css } from "twin.macro";
import { MouseEvent } from "react";

type ToggleBarPosition = "top" | "center" | "bottom";

interface ToggleBarProps {
  position: ToggleBarPosition,
  active: boolean
}

function ToggleBar({ position, active }: ToggleBarProps) {
  return (
    <span
      css={[
        tw`absolute left-0 w-full h-[2px] bg-black`,

        position === "top" && [
          tw`top-0 transition-transform`,
          css`
            transform-origin: center center;
          `,
          active
            ? css`
                transform: translateY(9.5px) rotate(45deg) translateZ(0);
              `
            : css`
                transform: none;
              `,
        ],

        position === "center" && [
          tw`transition-opacity`,
          css`
            top: 50%;
            transform: translateX(0%) translateY(-50%) translateZ(0px);
          `,
          active
            ? css`
                opacity: 0;
              `
            : css`
                opacity: 1;
              `,
        ],

        position === "bottom" && [
          tw`bottom-0 transition-transform`,
          css`
            transform-origin: center center;
          `,
          active
            ? css`
                transform: translateY(-9.5px) rotate(-45deg) translateZ(0);
              `
            : css`
                transform: none;
              `,
        ],
      ]}
    />
  );
}

interface NavigationToggleProps {
  active: boolean;
  onClick: (event: MouseEvent) => void;
}

export default function NavigationToggle({
  active,
  onClick,
}: NavigationToggleProps) {
  return (
    <div tw="lg:hidden flex items-center justify-center py-4">
      <button
        tw="relative w-[30px] h-[21px]"
        aria-label="Main menu"
        onClick={onClick}
      >
        <ToggleBar position="top" active={active} />
        <ToggleBar position="center" active={active} />
        <ToggleBar position="bottom" active={active} />
      </button>
    </div>
  );
}
