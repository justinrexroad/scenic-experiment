import tw from "twin.macro";
import { useState } from "react";
import useEventListener from "../../hooks/useEventListener";

export default function Dimensions() {
  const [dim, setDim] = useState(() =>
    typeof window !== "undefined"
      ? { width: window.innerWidth, height: window.innerHeight }
      : { width: 0, height: 0 }
  );

  useEventListener(
    "resize",
    () => {
      setDim({ width: window.innerWidth, height: window.innerHeight });
    },
    global
  );

  return (
    <div tw="mb-2">
      <span tw="text-lg font-semibold mr-2">Dimensions</span>
      {`${dim.width}px : ${dim.height}px`}
    </div>
  );
}
