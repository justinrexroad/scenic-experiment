import tw from "twin.macro";
import { useState } from "react";
import Breakpoint from "./Breakpoint";
import Dimensions from "./Dimensions";

export default function Panel() {
  const [toggleState, setToggleState] = useState(false);

  return (
    <div tw="absolute bottom-2 right-2 bg-gray-100 text-gray-700 px-4 py-2 rounded flex items-end">
      {toggleState && (
        <div tw="p-4">
          <Breakpoint />
          <Dimensions />
        </div>
      )}

      <div tw="flex">
        <button tw="ml-auto" onClick={() => setToggleState(!toggleState)}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 320 512"
            tw="h-4 w-auto inline-block"
          >
            <path
              fill="currentColor"
              d="M177 159.7l136 136c9.4 9.4 9.4 24.6 0 33.9l-22.6 22.6c-9.4 9.4-24.6 9.4-33.9 0L160 255.9l-96.4 96.4c-9.4 9.4-24.6 9.4-33.9 0L7 329.7c-9.4-9.4-9.4-24.6 0-33.9l136-136c9.4-9.5 24.6-9.5 34-.1z"
            ></path>
          </svg>
        </button>
      </div>
    </div>
  );
}
