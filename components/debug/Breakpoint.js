import tw, { css } from "twin.macro";

export default function Breakpoint() {
  return (
    <div tw="mb-2">
      <span tw="text-lg font-semibold mr-2">Breakpoint</span>
      <span tw="md:hidden">sm</span>
      <span tw="hidden md:inline lg:hidden">md</span>
      <span tw="hidden lg:inline xl:hidden">lg</span>
      <span tw="hidden xl:inline 2xl:hidden">xl</span>
      <span tw="hidden 2xl:inline">2xl</span>
    </div>
  );
}
