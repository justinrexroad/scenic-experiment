import { Dispatch, SetStateAction, useState } from "react";
import useEventListener from "./useEventListener";

const provider = {
  get(key: string, defaultValue = undefined) {
    const json = window.localStorage.getItem(key);
    return json === null || typeof json === "undefined"
      ? typeof defaultValue === "function"
        ? defaultValue()
        : defaultValue
      : JSON.parse(json);
  },
  set(key: string, value: string) {
    window.localStorage.setItem(key, value);
  },
};

export default function useLocalStorage<T>(key: string, initialValue: T) {
  // Default to useState if no localStorage
  if (!(typeof window !== "undefined" && window.localStorage)) {
    return useState<T>(initialValue);
  }

  useEventListener(
    "storage",
    ({ key: k, newValue }: StorageEvent) => {
      if (k === key) {
        const newState = JSON.parse(newValue);
        if (newState !== storedValue) {
          setStoredValue(newState);
        }
      }
    },
    global,
    { passive: true }
  );

  const [storedValue, setStoredValue] = useState<T>(() => provider.get(key));

  const setValue: Dispatch<SetStateAction<T>> = (value: SetStateAction<T>) => {
    const storeable = value instanceof Function ? value(storedValue) : value;
    setStoredValue(storeable);
    provider.set(key, JSON.stringify(storeable));
  };

  return [storedValue, setValue] as const;
}
