import { useEffect, useRef } from "react";

export default function useEventListener(
  type: string,
  listener: EventListener,
  target: EventTarget,
  options?: AddEventListenerOptions
) {
  const savedHandler = useRef<EventListener>();

  useEffect(() => {
    savedHandler.current = listener;
  }, [listener]);

  useEffect(() => {
    if (!target) return;

    const cb = (e) => savedHandler.current(e);

    target.addEventListener(type, cb, options);
    return () => target.removeEventListener(type, cb, options);
  }, [target, type, options]);
}
