import { GlobalStyles } from "twin.macro";
import DebugPanel from "../components/debug/Panel";

function MyApp({ Component, pageProps }) {
  return (
    <>
      <GlobalStyles />
      <Component {...pageProps} />
      {process.env.SHOW_DEBUG_PANEL === true && <DebugPanel />}
    </>
  );
}

export default MyApp;
