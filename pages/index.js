import Navbar from "../components/NavBar";
import tw from "twin.macro";

export default function Home() {
  return (
    <div>
      <Navbar />
      <DiscoverAdventure />
    </div>
  );
}

function DiscoverAdventure() {
  return (
    <div tw="relative">
      <picture>
        <source
          media="(max-width: 575.98px)"
          srcSet="https://d3hnd77n22bg9c.cloudfront.net/c/576w/c4156-4006-921-0/tardis/RS12200_stephen-walker-1533496-unsplash-stock-photo-partially-cloudy-day-grand-canyon-south-rim.jp2"
          type="image/jp2"
        />
        <source
          media="(max-width: 575.98px)"
          srcSet="https://d3hnd77n22bg9c.cloudfront.net/c/576w/c4156-4006-921-0/tardis/RS12200_stephen-walker-1533496-unsplash-stock-photo-partially-cloudy-day-grand-canyon-south-rim.webp"
          type="image/webp"
        />
        <source
          media="(max-width: 575.98px)"
          srcSet="https://d3hnd77n22bg9c.cloudfront.net/c/576w/c4156-4006-921-0/tardis/RS12200_stephen-walker-1533496-unsplash-stock-photo-partially-cloudy-day-grand-canyon-south-rim.jpg"
        />
        <source
          srcSet="https://d3hnd77n22bg9c.cloudfront.net/c/1920w/c6000-1250-0-1378/tardis/RS12200_stephen-walker-1533496-unsplash-stock-photo-partially-cloudy-day-grand-canyon-south-rim.jp2"
          type="image/jp2"
        />
        <source
          srcSet="https://d3hnd77n22bg9c.cloudfront.net/c/1920w/c6000-1250-0-1378/tardis/RS12200_stephen-walker-1533496-unsplash-stock-photo-partially-cloudy-day-grand-canyon-south-rim.webp"
          type="image/webp"
        />
        <img
          tw="object-cover w-full h-[400px] lg:h-[300px]"
          src="https://d3hnd77n22bg9c.cloudfront.net/c/1920w/c6000-1250-0-1378/tardis/RS12200_stephen-walker-1533496-unsplash-stock-photo-partially-cloudy-day-grand-canyon-south-rim.jpg"
        />
      </picture>
      <section id="home-cta"></section>
    </div>
  );
}
